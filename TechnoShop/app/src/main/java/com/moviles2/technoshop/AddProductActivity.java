package com.moviles2.technoshop;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.moviles2.technoshop.databinding.ActivityAddProductBinding;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class AddProductActivity extends AppCompatActivity {

    private ActivityAddProductBinding addProductBinding;
    private FirebaseStorage storage;
    private FirebaseFirestore db;
    private StorageReference storageReference;
    private ProgressDialog progressDialog;
    private Uri imageUri;
    private String imageDownloadUrl;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addProductBinding = ActivityAddProductBinding.inflate(getLayoutInflater());
        View v = addProductBinding.getRoot();
        setContentView(v);
        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference();
        db = FirebaseFirestore.getInstance();
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    public void selectImageFromGallery(View view){
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        galleryLauncher.launch(intent);
    }
    private ActivityResultLauncher<Intent> galleryLauncher = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            new ActivityResultCallback<ActivityResult>() {
                @Override
                public void onActivityResult(ActivityResult result) {
                    // obtenemos el resultaod de seleccionar la imagen
                    if(result.getResultCode() == Activity.RESULT_OK){
                        Intent data = result.getData();
                        Uri uri = data.getData();
                        if(uri != null){
                            addProductBinding.ivProduct.setImageURI(uri);
                            imageUri = uri;
                        }
                    }
                    else{
                        Toast.makeText(getApplicationContext(), "canceled", Toast.LENGTH_SHORT).show();
                    }
                }
            }
    );

    public void createProduct(View view){
        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Creando producto");
        progressDialog.show();
        SimpleDateFormat formatter = new SimpleDateFormat(
                "yyyy_MM_dd_HH_mm_ss.SSS", Locale.CANADA);
        Date date = new Date();
        String filename = formatter.format(date);
        // se asigna el nombre de la carpeta y nombre del archivo
        storageReference = FirebaseStorage.getInstance()
                .getReference("products/"+filename);
        // imagen a subir al storage
        UploadTask uploadTask = storageReference.putFile(imageUri);
        Task<Uri> urlTask = uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
            @Override
            public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                if(task.isSuccessful()){
                    return storageReference.getDownloadUrl();
                }
                else{
                    Toast.makeText(
                            getApplicationContext(),
                            "Problema subiendo imagen",
                            Toast.LENGTH_SHORT).show();
                    throw task.getException();
                }
            }
        }).addOnCompleteListener(new OnCompleteListener<Uri>() {
            @Override
            public void onComplete(@NonNull Task<Uri> task) {
                if(task.isSuccessful()){
                    Uri downloadUrl = task.getResult(); // obtiene la url de descarga
                    String url = String.valueOf(downloadUrl);
                    addProduct(url);
                    //Toast.makeText(getApplicationContext(), "" +downloadUrl.toString(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
    public void addProduct(String url){
        String productName = addProductBinding.etName.getText().toString();
        String description = addProductBinding.etDescription.getText().toString();
        String price = addProductBinding.etPrice.getText().toString();
        String stock = addProductBinding.etStock.getText().toString();
        String category = addProductBinding.etCategory.getText().toString();
        Map<String, Object> productData = new HashMap<>();
        productData.put("name", productName);
        productData.put("description", description);
        productData.put("price", Double.parseDouble(price));
        productData.put("stock", Integer.parseInt(stock));
        productData.put("category", category);
        productData.put("imageUrl", url);
        db.collection("products")
                .add(productData)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        progressDialog.dismiss();
                        Toast.makeText(getApplicationContext(), "Producto agregado", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(intent);
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(getApplicationContext(), "error", Toast.LENGTH_SHORT).show();
                    }
                });
    }
}