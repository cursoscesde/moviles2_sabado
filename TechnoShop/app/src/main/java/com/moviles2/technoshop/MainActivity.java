package com.moviles2.technoshop;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.moviles2.technoshop.Adapters.ProductAdapter;
import com.moviles2.technoshop.Entities.Product;
import com.moviles2.technoshop.databinding.ActivityMainBinding;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private ActivityMainBinding mainBinding;
    private FirebaseFirestore db;
    ArrayList<Product> productArrayList;
    ProductAdapter productAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mainBinding = ActivityMainBinding.inflate(getLayoutInflater());
        View view = mainBinding.getRoot();
        setContentView(view);
        db = FirebaseFirestore.getInstance();
        productArrayList = new ArrayList<>();
        productAdapter = new ProductAdapter(this,productArrayList, db);
        mainBinding.rvProducts.setHasFixedSize(true);
        mainBinding.rvProducts.setLayoutManager(new LinearLayoutManager(this));
        mainBinding.rvProducts.setAdapter(productAdapter);
        mainBinding.btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getProducts();
            }
        });
        //Intent intent = new Intent(this, AddProductActivity.class);
        //startActivity(intent);
        getProducts();
    }

    public void getProducts(){
        mainBinding.btnClear.setVisibility(View.GONE);
        db.collection("products")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if(task.isSuccessful()){
                            productArrayList.clear();
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                productArrayList.add(document.toObject(Product.class));
                            }
                            productAdapter.notifyDataSetChanged();

                        }
                        else{
                            Toast.makeText(
                                    getApplicationContext(),
                                    "No se pudo encontrar el producto",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    public void searchProduct(View view){
        mainBinding.btnClear.setVisibility(View.VISIBLE);
        String searchCriteria = mainBinding.etSearch.getText().toString();
        db.collection("products")
                .whereEqualTo("name",searchCriteria.toLowerCase())
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if(task.isSuccessful()){
                            productArrayList.clear();
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                productArrayList.add(document.toObject(Product.class));
                            }
                            productAdapter.notifyDataSetChanged();

                        }
                        else{
                            Toast.makeText(
                                    getApplicationContext(),
                                    "No se pudo encontrar el producto",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });

    }


}