package com.moviles2;

public class Dog extends Animal{
    private String tyHair;

    public Dog(int size) {
        super(size);
    }

    public String getTyHair() {
        return tyHair;
    }

    public void setTyHair(String tyHair) {
        this.tyHair = tyHair;
    }
    public void bite(){
        System.out.println("Mordiendo");
    }

    @Override
    public void makeSound() {
        System.out.println("Guau");
    }
}
