package com.moviles2;

import java.util.ArrayList;
import java.util.Scanner;

public class Shop {
    private Dog[] dogsDB;
    private Cat[] catsDB;
    private ArrayList<Cow> cowsDB;
    private Scanner inputData;
    public Shop(){
        dogsDB = new Dog[2];
        catsDB = new Cat[2];
        cowsDB = new ArrayList<Cow>();
        inputData = new Scanner(System.in);
    }

    public void registerDogs(){
        for(int i=0; i<dogsDB.length; i++){
            System.out.println("Ingrese tamaño: ");
            int size = Integer.parseInt(inputData.nextLine());
            Dog dog = new Dog(size);
            System.out.println("Ingrese Raza: ");
            String breed = inputData.nextLine();
            dog.setBreed(breed);
            System.out.println("Ingrese tipo de pelo: ");
            dog.setTyHair(inputData.nextLine());
            System.out.println("Ingrese género");
            dog.setGender(inputData.nextLine());
            dogsDB[i] = dog;
        }
    }

    public void listDogs(){
        for(int i=0; i<dogsDB.length; i++){
            System.out.println("Size: " +
                    dogsDB[i].getSize() +
                    " Breed: " + dogsDB[i].getBreed()+
                    " Type Hair: " + dogsDB[i].getTyHair() +
                    " Gener: " + dogsDB[i].getGender());
        }
    }
    public void registerCats(){
        for(int i=0; i<catsDB.length; i++){
            System.out.println("Ingrese tamaño: ");
            int size = Integer.parseInt(inputData.nextLine());
            Cat cat = new Cat(size);
            System.out.println("Ingrese Raza: ");
            String breed = inputData.nextLine();
            cat.setBreed(breed);
            System.out.println("Ingrese género");
            cat.setGender(inputData.nextLine());
            catsDB[i] = cat;
        }
    }

    public void registerCows(){
        System.out.println("ingrese el tamaño");
        int size = Integer.parseInt(inputData.nextLine());
        Cow cow = new Cow(size);
        System.out.println("Ingrese tipo de vaca");
        String typeCow = inputData.nextLine();
        cow.setTypeCow(typeCow);
        cowsDB.add(cow);
    }
    public void listCows(){
        for (int i=0; i<cowsDB.size(); i++){
            System.out.println("Type Cow: " + cowsDB.get(i).getTypeCow());
        }
    }

    public void listCats(){
        for(int i=0; i<catsDB.length; i++){
            System.out.println("Size: " +
                    catsDB[i].getSize() +
                    " Breed: " + catsDB[i].getBreed()+
                    " Gener: " + catsDB[i].getGender());
        }
    }
    public void menu(){
        boolean flag = true;
        while(flag) {
            System.out.println("Elejir del seguiemnt menú según preferencia");
            System.out.println("1 - registrar Gatos - 2 - " +
                    "registrar Perros - 3 listar gatos - 4 listar perros - 5 salir");
            int option = Integer.parseInt(inputData.nextLine());
            switch (option) {
                case 1:
                    registerCats();
                    break;
                case 2:
                    registerDogs();
                    break;
                case 3:
                    listCats();
                    break;
                case 4:
                    listDogs();
                    break;
                case 5:
                    registerCows();
                    break;
                case 6:
                    listCows();
                    break;
                case 7:
                    flag = false;
                    break;
            }
        }
    }
}
